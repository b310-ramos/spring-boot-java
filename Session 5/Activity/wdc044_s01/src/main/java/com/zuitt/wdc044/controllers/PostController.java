package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
// to indicate that a particular class is responsible for handling incoming HTTP requests and generating appropriate responses.

@CrossOrigin
// used in Java to enable Cross-Origin Resource Sharing (CORS) support in a Spring application. CORS is a security mechanism implemented by web browsers to restrict cross-origin requests made by web applications.
/*
When a client (e.g., a web browser) makes a request to a server, the server typically restricts access to its resources to requests originating from the same origin (i.e., the same domain, protocol, and port). This is known as the Same-Origin Policy enforced by web browsers.

However, there are cases where you might want to allow requests from a different origin to access your server's resources. This is where CORS comes into play. CORS allows controlled access to resources on a different origin.
* */
public class PostController {
    @Autowired
    PostService postService;

    //create a new post
    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @RequestMapping(value="/posts/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long id, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {
        return postService.updatePost(id, stringToken, post);
    }

    @RequestMapping(value="/posts/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long id, @RequestHeader(value = "Authorization") String stringToken) {
        return postService.deletePost(id, stringToken);
    }

    // @RequestMapping(value="/myPosts", method = RequestMethod.GET)
    // public ResponseEntity<Object> getAllUserPost(@RequestHeader(value = "Authorization") String stringToken) {
    //     return ResponseEntity<>(postService.getAllUserPost(stringToken), HttpStatus.OK);
    // }
    
    @RequestMapping(value="/myPosts", method = RequestMethod.GET)
    public ResponseEntity<List<Post>> getAllUserPosts(@RequestHeader(value = "Authorization") String stringToken) {
        List<Post> userPosts = postService.getAllUserPosts(stringToken);
        
        if (!userPosts.isEmpty()) {
            return new ResponseEntity<>(userPosts, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
